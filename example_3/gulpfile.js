/*global -$ */
'use strict';
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('serve', ['styles'], function () {
    browserSync({
        notify: false,
        port: 3000,
        server: {
            baseDir: ['.tmp', 'app'],
            routes: {
                '/bower_components': 'bower_components'
            }
        }
    });

    // watch for changes
    gulp.watch([
        'app/**/*.html',
    ]).on('change', reload);
    gulp.watch('app/styles/**/*.css', ['styles']);
});

gulp.task('styles', function () {
    return gulp.src('app/styles/main.css')
        .pipe($.sourcemaps.init())
        .pipe($.postcss([
            require('postcss-import')(),
            require('postcss-nested')(),
            require('rucksack-css')({
                fallbacks: true
            }),
            require('postcss-short')({
                'font-size': {
                    disable: true
                }
            }),
            require('cssnext')(),
            require('autoprefixer')({browsers: ['last 2 version']}),
            /*require('cssnano')(),*/
            require('postcss-reporter')({
                clearMessages: true,
            })
        ]))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest('.tmp/styles'))
        .pipe(reload({stream: true}));
});