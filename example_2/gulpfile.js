/*global -$ */
'use strict';
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('serve', ['styles'], function () {
    browserSync({
        notify: false,
        port: 3000,
        server: {
            baseDir: ['.tmp', 'app'],
            routes: {
                '/bower_components': 'bower_components'
            }
        }
    });

    // watch for changes
    gulp.watch([
        'app/**/*.html',
    ]).on('change', reload);
    gulp.watch('app/styles/**/*.scss', ['styles']);
});

gulp.task('styles', function () {
    return gulp.src('app/styles/main.scss')
        .pipe($.sass.sync().on('error', $.sass.logError))
        .pipe(gulp.dest('.tmp/styles'))
        .pipe(reload({stream: true}));
});