function example02(){

	function foo () { return 1 }
	// foo() === 1
	{
	    function foo () { return 2 }
	    //foo() === 2
	}
	// foo() === 1

	///////////////

	/* Variables */

	let x = 'unchanged';
	for(let i = 0; i < 3; i++){
	  let x = i;
	  console.log(x); // 1, 2, 3
	}
	console.log(x); // unchanged
	
}