function example06(){
    class Shape {
        constructor (id, x, y) {
            this.id = id;
            this.move(x, y);
        }
        move (x, y) {
            this.x = x;
            this.y = y;
        }
        printPosition(){
            console.log(this.x, this.y);
        }
        toString () {
            return `Shape(${this.id})`;
        }
    }

    class Rectangle extends Shape {
        constructor (id, x, y, width, height) {
            super(id, x, y);
            this.width  = width;
            this.height = height;
        }
        toString () {
            return "Rectangle > " + super.toString();
        }
    }
    class Circle extends Shape {
        constructor (id, x, y, radius) {
            super(id, x, y);
            this.radius = radius;
        }
        toString () {
            return "Circle > " + super.toString();
        }
        static defaultCircle () {
            return new Circle("default", 0, 0, 100)
        }
    }

    //////
    var square = new Shape('square', 0, 0);
    square.printPosition();
    square.move(10, 5);
    square.printPosition();
    //////
    var rectangle = new Rectangle('rectangle', 5, 7, 10, 10);
    console.log(rectangle.toString());
    var circle = new Circle('circle', 0, 0, 30);
    console.log(circle.toString());
    //////
    // var defaultCircle = Cirlce.defaultCircle();
    // console.log(defaultCircle.toString());
}