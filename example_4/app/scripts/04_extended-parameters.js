function example04(){
	// default parameters
	function f (x, y = 7, z = 42) {
	    return x + y + z;
	}
	console.log(f(1)); // 50

	//////////////////////////////
	// rest parameters
	function f (x, y, ...a) {
	    return (x + y) * a.length;
	}
	console.log(f(1, 2, "hello", true, 7)); // 9

	// spread parameter
	var str = "foo";
	var chars = [ ...str ];
	console.log(chars); // ['f', 'o', 'o']
}