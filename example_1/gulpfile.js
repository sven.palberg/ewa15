/*global -$ */
'use strict';
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('serve', function () {
    browserSync({
        notify: false,
        port: 3000,
        server: {
        	baseDir: ['app'],
        }
    });

    // watch for changes
    gulp.watch([
        'app/**/*.html',
    ]).on('change', reload);
    gulp.watch('app/styles/**/*.css', ['styles']);
});

gulp.task('styles', function () {
    return gulp.src('app/styles/**/*.css')
        .pipe(reload({stream: true}));
});